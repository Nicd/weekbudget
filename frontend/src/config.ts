/**
 * Remove errors from display after this many seconds.
 */
export const ERROR_TIMER = 10
