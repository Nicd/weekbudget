import { ImmutableModel } from './immutable-model'

export class BudgetEvent extends ImmutableModel {
  at: Date
  amount: number

  constructor(at: Date, amount: number) {
    super()

    this.at = at
    this.amount = amount
  }
}
