class ImmutableModelCopyError extends Error { }

export class ImmutableModel {
  makeImmutable(): this {
    Object.freeze(this)
    return this
  }

  copy(): ImmutableModel {
    throw new ImmutableModelCopyError(`Copy operation not implemented for ${this.constructor.name}.`)
  }
}
