import { ImmutableModel } from './immutable-model'
import { BudgetEvent } from './budget-event';

export class Budget extends ImmutableModel {
  uuid: string
  currency: string
  init: number
  amount: number
  events: BudgetEvent[]
  name: string

  constructor(uuid: string, currency: string, init: number, amount: number, events: Array<BudgetEvent>, name: string) {
    super()

    this.uuid = uuid
    this.currency = currency
    this.init = init
    this.amount = amount
    this.events = events
    this.name = name
  }

  copy(): Budget {
    return new Budget(
      this.uuid,
      this.currency,
      this.init,
      this.amount,
      this.events,
      this.name
    )
  }
}
