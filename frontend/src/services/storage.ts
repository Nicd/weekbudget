import { Budget } from './models/budget'

const LS_KEY = 'tinybudget-budgets'

export function storeToLocal(budgets: Budget[]) {
  const data = budgets.reduce((acc: string[], b) => [...acc, { uuid: b.uuid, name: b.name }], [])
  window.localStorage.setItem(LS_KEY, JSON.stringify(data))
}

export function getFromLocal(): { uuid: string, name: string }[] {
  const data = window.localStorage.getItem(LS_KEY) || '[]'
  return JSON.parse(data)
}
