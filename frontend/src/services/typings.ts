export type APIEvent = {
  amount: number,
  at: string
}

export type APIBudget = {
  uuid: string,
  init: number,
  amount: number,
  events: Array<APIEvent>,
  currency: string,
  name?: string // Name only exists when interfacing with localstorage
}
