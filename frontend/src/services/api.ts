import { APIBudget, APIEvent } from './typings'

class APIError extends Error { }

export class GetError extends APIError { }
export class CreationError extends APIError { }
export class ResetError extends APIError { }
export class EventCreationError extends APIError { }

const BASE_HEADERS = {
  'accept': 'application/json',
  'content-type': 'application/json'
}

export async function getBudget(uuid: string): Promise<APIBudget> {
  const resp = await fetch(_uuid2Path(uuid), {
    method: 'GET',
    headers: BASE_HEADERS
  })

  if (resp.status !== 200) {
    throw new GetError('Unable to read budget.')
  }

  return resp.json()
}

export async function createBudget(amount: number, currency: string): Promise<APIBudget> {
  const resp = await fetch('/budgets', {
    method: 'POST',
    headers: BASE_HEADERS,
    body: JSON.stringify({ amount, currency })
  })

  if (resp.status !== 201) {
    throw new CreationError('Unable to create budget.')
  }

  return resp.json()
}

export async function resetBudget(uuid: string, amount: number): Promise<APIBudget> {
  const resp = await fetch(_uuid2Path(uuid), {
    method: 'PATCH',
    headers: BASE_HEADERS,
    body: JSON.stringify({ amount })
  })

  if (resp.status !== 200) {
    throw new ResetError('Unable to reset budget.')
  }

  return resp.json()
}

export async function addEvent(uuid: string, amount: number): Promise<APIEvent> {
  const resp = await fetch(`${_uuid2Path(uuid)}/events`, {
    method: 'POST',
    headers: BASE_HEADERS,
    body: JSON.stringify({ amount })
  })

  if (resp.status !== 201) {
    throw new EventCreationError('Unable to create event.')
  }

  return resp.json()
}

function _uuid2Path(uuid: string) {
  return `/budgets/${uuid}`
}
