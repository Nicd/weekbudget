import { Budget } from './models/budget'
import { BudgetEvent } from './models/budget-event'
import { APIEvent, APIBudget } from './typings'

export function readEvent(data: APIEvent): BudgetEvent {
  const at = new Date(data.at)
  return new BudgetEvent(at, data.amount)
}

export function readBudget(data: APIBudget): Budget {
  const events: BudgetEvent[] = []

  for (const apiEvent of data.events) {
    events.push(readEvent(apiEvent).makeImmutable())
  }

  return new Budget(
    data.uuid,
    data.currency,
    data.init,
    data.amount,
    events,
    data.name || ''
  )
}
