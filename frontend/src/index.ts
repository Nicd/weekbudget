import './components/week-budget'
import './index.css'

window.onload = () => {
  const mainEl = document.getElementById('main-content')
  mainEl.innerHTML = '<week-budget></week-budget>'
}
