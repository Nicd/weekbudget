import { LitElement, html, css } from 'lit-element'
import * as QRCode from 'qrcode'

class BudgetQRComponent extends LitElement {
  uuid: string
  error: boolean

  static get properties() {
    return {
      uuid: { type: String },
      error: { type: Boolean }
    }
  }

  static get styles() {
    return css`
      p {
        font-size: 80%;
        font-weight: 200;
        margin: 10px 0;
      }
    `
  }

  constructor() {
    super()

    this.uuid = ''
    this.error = false
  }

  firstUpdated() {
    const root = <ShadowRoot>this.shadowRoot
    const canvas = root.getElementById(`${this.uuid}-qr-canvas`)

    QRCode.toCanvas(canvas, this.uuid, err => console.log(err))
  }

  render() {
    if (this.error) {
      return html`<p role="alert">There was an error rendering the QR code.</p>`
    }

    return html`
      <p role="alert" aria-label="Share this secret with another user:">${this.uuid}</p>
      <canvas id="${this.uuid}-qr-canvas"></canvas>
    `
  }
}

customElements.define('budget-qr', BudgetQRComponent)
