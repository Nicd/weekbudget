import { LitElement, html, css } from 'lit-element'

class Button extends LitElement {
  static get styles() {
    return css`
      button {
        border: 3px solid var(--dark-accent);
        border-radius: 5px;
        background-color: var(--dark-accent);
        color: var(--shaded-bright);
        font-size: 150%;
      }

      button:hover {
        filter: brightness(110%);
      }

      button:active {
        filter: brightness(90%);
      }
    `
  }

  render() {
    return html`
      <button type="button">
        <slot></slot>
      </button>
    `
  }
}

customElements.define('fa-button', Button)
