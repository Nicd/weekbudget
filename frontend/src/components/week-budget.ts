import { LitElement, html, css, TemplateResult } from 'lit-element'
import { Budget } from '../services/models/budget'
import { JoinBudgetEvent } from './budget-join'
import './budget-join'
import './budget-list'
import { readBudget, readEvent } from '../services/readers'
import { getBudget, GetError, addEvent, resetBudget, createBudget, CreationError } from '../services/api'
import { getFromLocal, storeToLocal } from '../services/storage'
import { SpendMoneyEvent, ResetBudgetEvent, RemoveBudgetEvent, RenameBudgetEvent } from './budget-display'
import { CreateBudgetEvent } from './budget-create'
import './budget-create'
import './button'
import './icon'

enum DisplayMode {
  BudgetView,
  CreateView,
  JoinView
}

class WeekBudgetComponent extends LitElement {
  displayMode: DisplayMode = DisplayMode.BudgetView

  budgets: Budget[] = []
  hasCreateError: boolean = false
  hasJoinError: boolean = false

  static get properties() {
    return {
      budgets: { type: Array },
      hasCreateError: { type: Boolean },
      hasJoinError: { type: Boolean },
      displayMode: { type: DisplayMode }
    }
  }

  static get styles() {
    return css`
      h1 {
        display: inline-flex;
        align-items: center;

        color: var(--light-accent);
      }
      
      h1 fa-button {
        margin-right: 10px;
      }

      hr {
        border: none;
        margin: 40px 20px;
        height: 1px;
        background-color: var(--shaded-bright);
      }

      .actions {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
        align-items: center;

        /* SUPER BIG */
        border-radius: 10px;
        font-size: 400%;
      }
    `
  }

  async firstUpdated() {
    const budgets = getFromLocal()
    for (const { uuid, name } of budgets) {
      try {
        const budget = await this._getBudget(uuid)
        const budgetCopy = budget.copy()
        budgetCopy.name = name || ''
        this.budgets = [...this.budgets, budgetCopy]
      }
      catch (err) {
        console.error(err)
      }
    }
  }

  async onCreateBudget(e: CreateBudgetEvent) {
    this.hasCreateError = false

    try {
      const budget = await createBudget(e.detail.amount, e.detail.currency)
      const budgetObj = readBudget(budget).copy()
      budgetObj.name = e.detail.name
      budgetObj.makeImmutable()
      this.budgets = [...this.budgets, budgetObj]
      storeToLocal(this.budgets)
      this.setDisplayMode(DisplayMode.BudgetView)
    }
    catch (err) {
      if (err instanceof CreationError) {
        this.hasCreateError = true
      }
    }
  }

  async onJoinBudget(e: JoinBudgetEvent) {
    this.hasJoinError = false

    if (this._findBudget(e.detail.uuid) !== -1) {
      // Budget has already been added
      return;
    }

    try {
      const budget = await this._getBudget(e.detail.uuid)
      this.budgets = [...this.budgets, budget]
      storeToLocal(this.budgets)
      this.setDisplayMode(DisplayMode.BudgetView)
    }
    catch (err) {
      if (err instanceof GetError) {
        this.hasJoinError = true
      }
    }
  }

  async onSpendMoney(e: SpendMoneyEvent) {
    try {
      const event = await addEvent(e.detail.budget.uuid, e.detail.amount)
      const eventObj = readEvent(event)

      const budgetIdx = this._findBudget(e.detail.budget.uuid)
      const budget = this.budgets[budgetIdx].copy()
      budget.amount -= e.detail.amount
      budget.events.unshift(eventObj)
      budget.makeImmutable()

      const budgetsCopy = [...this.budgets]
      budgetsCopy.splice(budgetIdx, 1, budget)
      this.budgets = budgetsCopy
    }
    catch (err) {
      console.error(err)
    }
  }

  async onResetBudget(e: ResetBudgetEvent) {
    try {
      const budget = await resetBudget(e.detail.budget.uuid, e.detail.amount)
      const budgetObj = readBudget(budget)

      const budgetIdx = this._findBudget(e.detail.budget.uuid)
      const budgetsCopy = [...this.budgets]
      budgetsCopy.splice(budgetIdx, 1, budgetObj)
      this.budgets = budgetsCopy
    }
    catch (err) {
      console.error(err)
    }
  }

  async onRenameBudget(e: RenameBudgetEvent) {
    const budgetIdx = this._findBudget(e.detail.budget.uuid)
    const budgetObj = this.budgets[budgetIdx].copy()
    budgetObj.name = e.detail.name
    budgetObj.makeImmutable()

    const budgetsCopy = [...this.budgets]
    budgetsCopy.splice(budgetIdx, 1, budgetObj)
    this.budgets = budgetsCopy
    storeToLocal(this.budgets)
  }

  async onRemoveBudget(e: RemoveBudgetEvent) {
    const budgetIdx = this._findBudget(e.detail.budget.uuid)
    const budgetsCopy = [...this.budgets]
    budgetsCopy.splice(budgetIdx, 1)
    this.budgets = budgetsCopy
    storeToLocal(this.budgets)
  }

  setDisplayMode(mode: DisplayMode) {
    this.displayMode = mode
  }

  render() {
    const headingWithContent = (content: TemplateResult, text: string) => html`<h1>${content}${text}</h1>`
    const back = html`
      <fa-button @click=${() => this.setDisplayMode(DisplayMode.BudgetView)}>
        <fa-icon name="arrow-left" alt="Go back"></fa-icon>
      </fa-button>
    `

    switch (this.displayMode) {
      case DisplayMode.CreateView:
        return html`
          ${headingWithContent(back, 'Create budget')}
          <budget-create
            @createBudget=${this.onCreateBudget}
            .hasError=${this.hasCreateError}
          ></budget-create>
        `

      case DisplayMode.JoinView:
        return html`
          ${headingWithContent(back, 'Join budget')}
          <budget-join
            @joinBudget=${this.onJoinBudget}
            .hasError=${this.hasJoinError}
          ></budget-join>
        `

      default:
        return html`
          ${headingWithContent(html``, 'TinyBudget')}
          <budget-list
            @spendMoney=${this.onSpendMoney}
            @resetBudget=${this.onResetBudget}
            @renameBudget=${this.onRenameBudget}
            @removeBudget=${this.onRemoveBudget}
            .budgets=${this.budgets}
          ></budget-list>

          <hr aria-hidden="true" />

          <div class="actions">
            <fa-button @click=${() => this.setDisplayMode(DisplayMode.CreateView)}>
              <fa-icon name="plus" alt="Create new budget"></fa-icon>
            </fa-button>

            <fa-button @click=${() => this.setDisplayMode(DisplayMode.JoinView)}>
              <fa-icon name="user-friends" alt="Join existing budget"></fa-icon>
            </fa-button>
          </div>
        `
    }
  }

  async _getBudget(uuid: string): Promise<Budget> {
    return readBudget(await getBudget(uuid)).makeImmutable()
  }

  _findBudget(uuid: string) {
    return this.budgets.findIndex(b => b.uuid === uuid)
  }
}

customElements.define('week-budget', WeekBudgetComponent)
