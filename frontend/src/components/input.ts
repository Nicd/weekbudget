import { LitElement, css, html } from 'lit-element'

export class InputField extends LitElement {
  id: string
  type: string
  placeholder: string
  maxlength: number
  pattern: string
  required: boolean
  label: string

  static get properties() {
    return {
      id: { type: String },
      type: { type: String },
      placeholder: { type: String },
      maxlength: { type: Number },
      pattern: { type: String },
      required: { type: Boolean },
      label: { type: String }
    }
  }

  static get styles() {
    return css`
      :host {
        display: flex;
        flex-direction: column;

        margin: 10px 0;
      }

      label {
        margin-bottom: 5px;
      }

      input {
        border: 3px solid var(--dark-accent);
        border-radius: 5px;
        background-color: var(--bg-color);
        color: var(--shaded-bright);
        font-size: 150%;
      }
    `
  }

  get inputId() {
    return `${this.id}-input`
  }

  getValue() {
    return (<HTMLInputElement>this.shadowRoot.getElementById(this.inputId)).value
  }

  clearValue() {
    (<HTMLInputElement>this.shadowRoot.getElementById(this.inputId)).value = ''
  }

  async focusInput() {
    await this.updateComplete
    const el = <HTMLInputElement>this.shadowRoot.getElementById(this.inputId)
    el.focus()
  }

  render() {
    return html`
      <label for="${this.inputId}">${this.label}</label>
      <input
        id=${this.inputId}
        type=${this.type}
        placeholder=${this.placeholder ? this.placeholder : ''}
        maxlength=${this.maxlength}
        pattern=${this.pattern}
        ?required=${this.required}
      />
    `
  }
}

customElements.define('input-field', InputField)
