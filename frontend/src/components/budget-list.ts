import './budget-display'
import { LitElement, html, css } from 'lit-element'
import { Budget } from '../services/models/budget'
import { MOB_BP } from './styles'

class BudgetListComponent extends LitElement {
  budgets: Budget[]

  static get properties() {
    return {
      budgets: { type: Array }
    }
  }

  static get styles() {
    return css`
      #list {
        display: grid;
        grid-template-columns: 1fr 1fr;
        gap: 20px;
      }

      ${MOB_BP} {
        #list {
          grid-template-columns: auto;
        }
      }
    `
  }

  constructor() {
    super()

    this.budgets = []
  }

  render() {
    return html`
      <div id="list" aria-label="List of budgets">
        ${this.budgets.map(b => html`<budget-display .budget=${b}></budget-display>`)}
      </div>
    `
  }
}

customElements.define('budget-list', BudgetListComponent)
