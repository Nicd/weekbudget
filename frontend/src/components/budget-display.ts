import { LitElement, html, css } from 'lit-element'
import { Budget } from '../services/models/budget'
import './budget-qr'
import './button'
import './icon'
import { BudgetEvent } from '../services/models/budget-event'

export class SpendMoneyEvent extends CustomEvent<{ budget: Budget, amount: number }> { }

export class ResetBudgetEvent extends CustomEvent<{ budget: Budget, amount: number }> { }

export class RemoveBudgetEvent extends CustomEvent<{ budget: Budget }> { }

export class RenameBudgetEvent extends CustomEvent<{ budget: Budget, name: string }> { }

class BudgetDisplayComponent extends LitElement {
  budget: Budget
  showShare: boolean

  static get properties() {
    return {
      budget: { type: Budget },
      showShare: { type: Boolean }
    }
  }

  static get styles() {
    return css`
      :host {
        display: block;
        justify-self: center;
      }

      h3 {
        font-size: 150%;
        margin: 10px 0 0 0;
      }

      h5 {
        margin-bottom: 5px;
        border-bottom: 1px solid var(--shaded-bright);
      }

      p {
        margin: 0;
      }

      .amounts {
        display: grid;
        column-gap: 10px;
        grid-template: 'now curr' 'of init' / auto 66%;

        margin-top: 10px;
      }

      .money-left {
        grid-area: now;
        font-size: 200%;
        text-align: right;
      }

      .money-left[data-negative="t"] {
        color: var(--danger-color);
      }

      .money-separator {
        grid-area: of;
        text-transform: uppercase;
        text-align: right;
      }

      .money-initial {
        grid-area: init;
      }

      .money-currency {
        grid-area: curr;
        align-self: center;
      }

      .events {
        list-style-type: none;
        padding: 0;
        margin-top: 0;
      }

      .event {
        display: grid;
        grid-template-columns: auto 66%;
        column-gap: 10px;
      }

      .event-date {
        text-align: right;
      }
    `
  }

  constructor() {
    super()

    this.budget = null
    this.showShare = false
  }

  onSpendMoney() {
    const amount = parseFloat(prompt('Amount spent (use negative to receive money instead)'))

    if (isNaN(amount)) {
      return
    }

    this.dispatchEvent(new SpendMoneyEvent(
      'spendMoney', { detail: { budget: this.budget, amount }, bubbles: true, composed: true }
    ))
  }

  onReset() {
    const amount = parseFloat(prompt('Reset budget amount to (will delete all events)'))

    if (isNaN(amount)) {
      return
    }

    this.dispatchEvent(new ResetBudgetEvent(
      'resetBudget', { detail: { budget: this.budget, amount }, bubbles: true, composed: true }
    ))
  }

  onRename() {
    const newName = prompt('Give new name for budget')

    if (newName == null) {
      return
    }

    this.dispatchEvent(new RenameBudgetEvent(
      'renameBudget',
      { detail: { budget: this.budget, name: newName }, bubbles: true, composed: true }
    ))
  }

  onRemove() {
    if (!confirm('Do you really wish to remove the budget?')) {
      return
    }

    this.dispatchEvent(new RemoveBudgetEvent(
      'removeBudget', { detail: { budget: this.budget }, bubbles: true, composed: true }
    ))
  }

  toggleShare() {
    this.showShare = !this.showShare
  }

  render() {
    if (!this.budget) {
      return html``
    }

    return html`
      <h3>${this.budget.name != '' ? this.budget.name : 'Unnamed budget'}</h3>

      <div
        class="amounts"
        aria-label="${this.budget.amount.toFixed(2)} left of ${this.budget.init.toFixed(2)} ${this.budget.currency}."
      >
        <p
          class="money-left"
          aria-hidden="true"
          data-negative="${this.budget.amount < 0 ? 't' : 'f'}"
        >${this.budget.amount.toFixed(2)}</p>
        <p class="money-separator" aria-hidden="true">of</p>
        <p class="money-initial" aria-hidden="true">${this.budget.init.toFixed(2)}</p>
        <p class="money-currency" aria-hidden="true">${this.budget.currency}</p>
      </div>

      <h5>Events</h5>

      <ul class="events">
        ${this.budget.events.map(event => this._renderEvent(this.budget, event))}
      </ul>

      <div class="actions" aria-label="Actions for budget '${this.budget.name}'">
        <fa-button @click=${this.onSpendMoney}>
          <fa-icon name="credit-card" alt="Spend money"></fa-icon>
        </fa-button>
        <fa-button @click=${this.onReset}>
          <fa-icon name="undo" alt="Reset to given amount"></fa-icon>
        </fa-button>
        <fa-button @click=${this.onRename}>
          <fa-icon name="edit" alt="Rename"></fa-icon>
        </fa-button>
        <fa-button @click=${this.toggleShare}>
          <fa-icon name="share-alt" alt="Share"></fa-icon>
        </fa-button>
        <fa-button @click=${this.onRemove}>
          <fa-icon name="trash" alt="Remove"></fa-icon>
        </fa-button>
      </div>

      <div>
        ${this.showShare ? html`<budget-qr .uuid=${this.budget.uuid}></budget-qr>` : null}
      </div>
    `
  }

  _renderEvent(budget: Budget, event: BudgetEvent) {
    return html`
      <li
        class="event"
        aria-label="${event.amount.toFixed(2)} ${budget.currency} on ${this._formatDate(event.at)}"
      >
        <p class="event-date" aria-hidden="true">${this._formatDate(event.at)}</p>
        <p class="event-amount" aria-hidden="true">${event.amount.toFixed(2)}</p>
      </li>
    `
  }

  _formatDate(date: Date) {
    return date.toLocaleDateString("en-GB", { day: 'numeric', month: 'long' })
  }
}

customElements.define('budget-display', BudgetDisplayComponent)
