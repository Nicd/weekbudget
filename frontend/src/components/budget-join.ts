import { LitElement, html } from 'lit-element'
import { InputField } from './input'
import './budget-qr-reader'
import { QRReadEvent } from './budget-qr-reader'
import { ERROR_TIMER } from '../config'

export class JoinBudgetEvent extends CustomEvent<{ uuid: string }> { }

class BudgetJoinComponent extends LitElement {
  hasError: boolean = false
  showQRReader: boolean = false
  errorTimer: number = null

  static get properties() {
    return {
      hasError: { type: Boolean },
      showQRReader: { type: Boolean }
    }
  }

  firstUpdated() {
    const secretInputEl = <InputField>this.shadowRoot.getElementById('uuid')
    secretInputEl.focusInput()
  }

  updated(changedProperties: Map<string | number | symbol, any>) {
    // If error was turned on, turn it off after a moment
    if (changedProperties.has('hasError')) {
      if (this.errorTimer != null) {
        window.clearTimeout(this.errorTimer)
      }

      this.errorTimer = window.setTimeout(() => {
        this.hasError = false
        this.errorTimer = null
      }, ERROR_TIMER * 1000)
    }

    return super.updated(changedProperties)
  }

  onJoin(e: Event) {
    e.preventDefault()
    const inputEl = <InputField>this.shadowRoot.getElementById('uuid')

    this.dispatchEvent(new JoinBudgetEvent(
      'joinBudget',
      { detail: { uuid: inputEl.getValue() } }
    ))

    inputEl.clearValue()
  }

  onQRRead(e: QRReadEvent) {
    const text = e.detail.text
    this.dispatchEvent(new JoinBudgetEvent(
      'joinBudget',
      { detail: { uuid: text } }
    ))
    this.showQRReader = false
  }

  toggleShowQRReader() {
    this.showQRReader = !this.showQRReader
  }

  render() {
    return html`
      <input-field
        id="uuid"
        type="text"
        label="Budget secret (ask someone to share theirs)"
        maxlength="36"
        pattern="[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-4[0-9A-Fa-f]{3}-[89ABab][0-9A-Fa-f]{3}-[0-9A-Fa-f]{12}"
        required
      ></input-field>
      <fa-button @click=${this.onJoin}>
        <fa-icon name="plus" alt="Join budget"></fa-icon>
      </fa-button>
      <fa-button type="button" @click=${this.toggleShowQRReader}>
        <fa-icon
          name="qrcode"
          alt="${(!this.showQRReader) ? 'Read QR using camera' : 'Stop reading QR'}"
        ></fa-icon>
      </fa-button>

      <div>
        ${this.showQRReader ? html`<budget-qr-reader @qrRead=${this.onQRRead}></budget-qr-reader>` : null}
      </div>

      ${this.hasError ? html`<p role="alert">Could not add given budget, please check it is correctly typed.</p>` : null}
    `
  }
}

customElements.define('budget-join', BudgetJoinComponent)
