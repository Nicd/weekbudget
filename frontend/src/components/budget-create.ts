import { LitElement, html, css } from 'lit-element'
import { InputField } from './input'
import './input'
import { MOB_BP } from './styles';
import { ERROR_TIMER } from '../config';

export class CreateBudgetEvent extends CustomEvent<{
  amount: number,
  currency: string,
  name: string
}> { }

class BudgetCreateComponent extends LitElement {
  hasError: boolean = false
  errorTimer: number = null

  static get properties() {
    return {
      hasError: { type: Boolean }
    }
  }

  static get styles() {
    return css`
      :host {
        display: grid;
        grid-template-columns: 1fr 1fr 1fr;
        column-gap: 10px;
      }

      ${MOB_BP} {
        :host {
          grid-template-columns: auto;
        }
      }

      p {
        margin: 0;
      }
    `
  }

  firstUpdated() {
    const nameInputEl = <InputField>this.shadowRoot.getElementById('name')
    nameInputEl.focusInput()
  }

  updated(changedProperties: Map<string | number | symbol, any>) {
    // If error was turned on, turn it off after a moment
    if (changedProperties.has('hasError')) {
      if (this.errorTimer != null) {
        window.clearTimeout(this.errorTimer)
      }

      this.errorTimer = window.setTimeout(() => {
        this.hasError = false
        this.errorTimer = null
      }, ERROR_TIMER * 1000)
    }

    return super.updated(changedProperties)
  }

  onCreate(e: Event) {
    e.preventDefault()
    const root = this.shadowRoot
    const amountInputEl = <InputField>root.getElementById('amount')
    const currencyInputEl = <InputField>root.getElementById('currency')
    const nameInputEl = <InputField>root.getElementById('name')

    const name = nameInputEl.getValue().trim()
    const amount = parseInt(amountInputEl.getValue())
    if (isNaN(amount)) {
      this.hasError = true
      return
    }

    this.dispatchEvent(new CreateBudgetEvent(
      'createBudget',
      { detail: { amount, currency: currencyInputEl.getValue(), name } }
    ))

    amountInputEl.clearValue()
    currencyInputEl.clearValue()
    nameInputEl.clearValue()
  }

  render() {
    return html`
      <input-field
        id="name"
        type="text"
        label="Name (optional)"
      ></input-field>
      <input-field
        id="amount"
        type="number"
        label="Amount"
        required
      ></input-field>
      <input-field
        id="currency"
        type="text"
        label="Currency (3 chars)"
        pattern="[A-Z]{3}"
        maxlength="3"
        required
      ></input-field>
      <fa-button @click=${this.onCreate}>Create budget</fa-button>

      ${this.hasError ? html`<p role="alert">Could not create budget, check the amount and currency.</p>` : null}
    `
  }
}

customElements.define('budget-create', BudgetCreateComponent)
