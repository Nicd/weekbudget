import { LitElement, html, css } from 'lit-element'
import { BrowserQRCodeReader, VideoInputDevice } from '../vendor/zxing-typescript/src/browser/BrowserQRCodeReader'
import { SR_ONLY } from './styles'

export class QRReadEvent extends CustomEvent<{ text: string }> { }

const UUID_RE = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/

class BudgetQRReaderComponent extends LitElement {
  reader: BrowserQRCodeReader
  inputs: VideoInputDevice[]
  currentInput: string
  currentInputLabel: string
  test: string
  scanActive: boolean

  static get properties() {
    return {
      inputs: { type: Array },
      currentInput: { type: String },
      currentInputLabel: { type: String },
      test: { type: String },
      scanActive: { type: Boolean }
    }
  }

  static get styles() {
    return css`
      ${SR_ONLY}
    `
  }

  constructor() {
    super()

    this.reader = new BrowserQRCodeReader()
    this.inputs = []
    this.currentInput = null
  }

  async firstUpdated() {
    this.inputs = await this.reader.getVideoInputDevices()
    if (this.inputs.length > 0) {
      this.currentInput = this.inputs[0].deviceId
      this.currentInputLabel = this.inputs[0].label
      this.startRead()
    }
  }

  disconnectedCallback() {
    this.reader.reset()
  }

  renderVideoInput(input: VideoInputDevice) {
    return html`
      <option
        value=${input.deviceId}
        ?selected=${this.currentInput === input.deviceId}
      >${input.label}</option>
    `
  }

  async startRead() {
    const root = this.shadowRoot
    const videoEl = <HTMLVideoElement>root.getElementById('video')

    let found = false
    do {
      this.scanActive = true
      const result = await this.reader.decodeFromInputVideoDevice(this.currentInput, videoEl)
      const text = result.getText()
      if (this._isUUID(text)) {
        this.dispatchEvent(new QRReadEvent(
          'qrRead',
          { detail: { text } }
        ))
        found = true
      }
    } while (!found)
  }

  stopRead() {
    this.scanActive = false
    this.reader.reset()
  }

  inputChanged() {
    const inputEl = <HTMLSelectElement>this.shadowRoot.getElementById('input')
    this.currentInput = inputEl.value
    this.currentInputLabel = inputEl.options[inputEl.selectedIndex].label
    this.stopRead()
    this.startRead()
  }

  render() {
    return html`
      ${this.scanActive ? html`<p role="alert" class="sr-only">Now scanning for QR codes with device ${this.currentInputLabel}.</p>` : null}
      <video id="video" width="300" height="200" style="border: 1px solid gray"></video>
      <select id="input" @change="${this.inputChanged}">
        ${this.inputs.map(input => this.renderVideoInput(input))}
      </select>
    `
  }

  _isUUID(text: string) {
    return UUID_RE.test(text)
  }
}

customElements.define('budget-qr-reader', BudgetQRReaderComponent)
