import { css } from 'lit-element'

/**
 * Breakpoint media query to use for mobile layouts.
 */
export const MOB_BP = css`@media (max-width: 768px)`

/** Show something only for screen readers. */
export const SR_ONLY = css`
  /* From https://webaim.org/techniques/css/invisiblecontent/ */
  .sr-only {
    clip: rect(1px, 1px, 1px, 1px);
    clip-path: inset(50%);
    height: 1px;
    width: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
  }
`
