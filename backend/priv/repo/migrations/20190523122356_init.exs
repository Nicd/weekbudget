defmodule WeekBudget.DB.Repo.Migrations.Init do
  use Ecto.Migration

  def change do
    create table(:budgets, primary_key: false) do
      add(:secret, :binary_id, primary_key: true)
      add(:amount, :money_with_currency)

      timestamps()
    end
  end
end
