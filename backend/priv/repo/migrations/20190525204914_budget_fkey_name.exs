defmodule WeekBudget.DB.Repo.Migrations.BudgetFkeyName do
  use Ecto.Migration

  def change do
    rename(table("events"), :budget, to: :budget_id)
  end
end
