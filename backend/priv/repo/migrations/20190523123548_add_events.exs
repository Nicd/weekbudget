defmodule WeekBudget.DB.Repo.Migrations.AddEvents do
  use Ecto.Migration

  def change do
    create table(:events) do
      add(:amount, :money_with_currency, null: false)
      add(:at, :utc_datetime, null: false)

      add(
        :budget,
        references("budgets", column: :secret, type: :binary_id, on_delete: :delete_all),
        null: false
      )
    end

    create(index("events", :budget))
  end
end
