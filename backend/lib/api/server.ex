defmodule WeekBudget.API.Server do
  @moduledoc """
  Simple server handling/delegating all API calls of the system.
  """

  require Logger
  use Ace.HTTP.Service, port: 2019, cleartext: true
  use Raxx.SimpleServer

  @impl Raxx.SimpleServer
  @spec handle_request(Raxx.Request.t(), %{}) :: Raxx.Response.t()
  def handle_request(req, state)

  # GET budget information
  def handle_request(%{method: :GET, path: ["budgets", uuid]}, _state) when uuid != "" do
    with {:ok, _} <- Ecto.UUID.dump(uuid),
         %WeekBudget.DB.Budget{} = wb <- WeekBudget.DB.Budget.get_by_secret(uuid) do
      wb
      |> serialize_budget()
      |> json_resp()
    else
      :error -> json_resp(:bad_request, %{error: "Malformed UUID."})
      nil -> json_resp(:not_found, %{error: "Budget not found."})
    end
  end

  # POST new event into budget
  def handle_request(%{method: :POST, path: ["budgets", uuid, "events"], body: body}, _state)
      when uuid != "" and body != "" do
    {status, data} =
      with {:uuid, {:ok, _}} <- {:uuid, Ecto.UUID.dump(uuid)},
           {:wb, %WeekBudget.DB.Budget{} = wb} <- {:wb, WeekBudget.DB.Budget.get_by_secret(uuid)},
           {:json, {:ok, %{"amount" => amnt}}} when is_number(amnt) <-
             {:json, Jason.decode(body)},
           {:money, %Money{} = money} <-
             {:money, WeekBudget.Core.MoneyUtils.create(wb.amount.currency, -amnt)} do
        {:ok, event} = WeekBudget.DB.Event.create(wb, money)
        event_json = serialize_event(event)
        {:created, event_json}
      else
        {:uuid, _} -> {:bad_request, %{error: "Malformed UUID."}}
        {:wb, _} -> {:not_found, %{error: "Budget not found."}}
        {:json, _} -> {:bad_request, %{error: "Cannot parse JSON."}}
        {:currency, _} -> {:bad_request, %{error: "Invalid currency."}}
        {:money, _} -> {:internal_server_error, %{error: "Cannot create money."}}
      end

    json_resp(status, data)
  end

  # PATCH budget with a new amount, deleting all events
  def handle_request(%{method: :PATCH, path: ["budgets", uuid], body: body}, _state)
      when uuid != "" and body != "" do
    {status, data} =
      with {:uuid, {:ok, _}} <- {:uuid, Ecto.UUID.dump(uuid)},
           {:wb, %WeekBudget.DB.Budget{} = wb} <- {:wb, WeekBudget.DB.Budget.get_by_secret(uuid)},
           {:json, {:ok, %{"amount" => amnt}}} when is_number(amnt) <-
             {:json, Jason.decode(body)},
           {:money, %Money{} = money} <-
             {:money, WeekBudget.Core.MoneyUtils.create(wb.amount.currency, amnt)} do
        {:ok, %{amount_update: budget}} = WeekBudget.DB.Budget.reset(wb, money)
        budget_json = serialize_budget(%WeekBudget.DB.Budget{budget | events: []})
        {:ok, budget_json}
      else
        {:uuid, _} -> {:bad_request, %{error: "Malformed UUID."}}
        {:wb, _} -> {:not_found, %{error: "Budget not found."}}
        {:json, _} -> {:bad_request, %{error: "Cannot parse JSON."}}
        {:currency, _} -> {:bad_request, %{error: "Invalid currency."}}
        {:money, _} -> {:internal_server_error, %{error: "Cannot create money."}}
      end

    json_resp(status, data)
  end

  # POST to create a new budget
  def handle_request(%{method: :POST, path: ["budgets"], body: body}, _state) when body != "" do
    {status, data} =
      with {:json, {:ok, %{"amount" => amnt, "currency" => curr_str}}}
           when is_number(amnt) and is_binary(curr_str) <- {:json, Jason.decode(body)},
           {:currency, {:ok, curr}} <- {:currency, Money.validate_currency(curr_str)},
           {:money, %Money{} = money} <- {:money, WeekBudget.Core.MoneyUtils.create(curr, amnt)} do
        {:ok, budget} = WeekBudget.DB.Budget.create(money)
        budget_json = serialize_budget(%WeekBudget.DB.Budget{budget | events: []})
        {:created, budget_json}
      else
        {:json, _} -> {:bad_request, %{error: "Cannot parse JSON."}}
        {:currency, _} -> {:bad_request, %{error: "Invalid currency."}}
        {:money, _} -> {:internal_server_error, %{error: "Cannot create money."}}
      end

    json_resp(status, data)
  end

  def handle_request(req, _) do
    Logger.debug("Failed request:")
    Logger.debug(inspect(req))
    json_resp(:not_found, %{error: "Unknown API path or invalid data."})
  end

  defp serialize_budget(%WeekBudget.DB.Budget{secret: secret, amount: amount, events: events}) do
    # True amount is initial amount plus all event amounts
    true_amount = Enum.reduce(events, amount, &Money.add!(&2, &1.amount))

    %{
      uuid: secret,
      init: Decimal.to_float(amount.amount),
      amount: Decimal.to_float(true_amount.amount),
      events: Enum.map(events, &serialize_event/1),
      currency: amount.currency
    }
  end

  defp serialize_event(%WeekBudget.DB.Event{at: at, amount: amount}) do
    %{at: DateTime.to_iso8601(at), amount: Decimal.to_float(amount.amount)}
  end

  defp json_resp(status \\ :ok, content) do
    response(status)
    |> set_header("content-type", "application/json")
    |> set_body(Jason.encode!(content))
  end
end
