defmodule WeekBudget.DB.Event do
  @moduledoc """
  A money spending event that occurred on the budget. If amount is positive, money was spent. If amount
  is negative, money was received.
  """
  use Ecto.Schema

  schema "events" do
    field(:amount, Money.Ecto.Composite.Type)
    field(:at, :utc_datetime)

    belongs_to(:budget, WeekBudget.DB.Budget,
      references: :secret,
      foreign_key: :budget_id,
      type: :binary_id
    )
  end

  @doc """
  Create a new event with the given amount. Note that normally the amount should be negative.
  """
  @spec create(WeekBudget.DB.Budget.t(), Money.t()) ::
          {:ok, __MODULE__.t()} | {:error, Ecto.Changeset.t()}
  def create(%WeekBudget.DB.Budget{} = budget, %Money{} = amount) do
    %__MODULE__{
      amount: amount,
      budget: budget,
      at: DateTime.utc_now() |> DateTime.truncate(:second)
    }
    |> WeekBudget.DB.Repo.insert()
  end
end
