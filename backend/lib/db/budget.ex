defmodule WeekBudget.DB.Budget do
  @moduledoc """
  A single budget that has a secret token that can be used to join it and an initial sum.

  The final sum can be calculated by subtracting all the events from the initial sum.
  """
  use Ecto.Schema
  import Ecto.Query, only: [from: 2]

  @primary_key {:secret, :binary_id, autogenerate: true}

  schema "budgets" do
    field(:amount, Money.Ecto.Composite.Type)

    timestamps()

    has_many(:events, WeekBudget.DB.Event, foreign_key: :budget_id)
  end

  @doc """
  Get budget by its secret, if any exists.
  """
  @spec get_by_secret(String.t()) :: __MODULE__.t() | nil
  def get_by_secret(secret) do
    events_q = from(e in WeekBudget.DB.Event, order_by: [desc: e.at])

    from(w in __MODULE__,
      where: w.secret == ^secret,
      select: [:secret, :amount],
      preload: [events: ^events_q]
    )
    |> WeekBudget.DB.Repo.one()
  end

  @doc """
  Create budget with given initial amount.
  """
  @spec create(Money.t()) :: {:ok, __MODULE__.t()} | {:error, Ecto.Changeset.t()}
  def create(amount) do
    %__MODULE__{
      amount: amount
    }
    |> WeekBudget.DB.Repo.insert()
  end

  @doc """
  Reset budget to given amount an delete all events.
  """
  @spec reset(__MODULE__.t(), Money.t()) ::
          {:ok, %{amount_update: __MODULE__.t(), delete_events: any()}} | {:error, any()}
  def reset(budget, amount) do
    update_cset =
      budget
      |> Ecto.Changeset.cast(%{amount: amount}, [:amount])

    Ecto.Multi.new()
    |> Ecto.Multi.update(:amount_update, update_cset)
    |> Ecto.Multi.delete_all(:delete_events, Ecto.assoc(budget, :events))
    |> WeekBudget.DB.Repo.transaction()
  end
end
