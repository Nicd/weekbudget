defmodule WeekBudget.DB.Repo do
  use Ecto.Repo,
    otp_app: :week_budget,
    adapter: Ecto.Adapters.Postgres

  def init(_type, config) do
    {:ok, Keyword.put(config, :url, Application.get_env(:week_budget, :database_url))}
  end
end
