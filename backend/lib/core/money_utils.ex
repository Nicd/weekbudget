defmodule WeekBudget.Core.MoneyUtils do
  @moduledoc """
  Utilities to do with money operations.
  """

  @doc """
  Create money from given integer or float and currency. Float is used to easier accept money from the
  frontends. It is normalised to a Money instance at first opportunity so it is believed that the
  impact and potential for errors is minimal.
  """
  @spec create(atom | String.t(), integer | float) :: Money.t() | {:error, {module, String.t()}}
  def create(currency, amount)

  def create(currency, amount) when is_integer(amount) do
    Money.new(currency, amount)
  end

  def create(currency, amount) when is_float(amount) do
    Money.from_float(currency, amount)
  end
end
