defmodule WeekBudget.Core.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    case Code.ensure_loaded(ExSync) do
      {:module, ExSync = mod} ->
        mod.start()

      {:error, _} ->
        :ok
    end

    port = Application.get_env(:week_budget, :port, 2019)

    # List all child processes to be supervised
    children = [
      {WeekBudget.DB.Repo, []},
      {WeekBudget.API.Server, [[], [port: port]]}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: WeekBudget.Core.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
