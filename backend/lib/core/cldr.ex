defmodule WeekBudget.Core.Cldr do
  use Cldr, locales: ["en"], default_locale: "en"
end
