defmodule WeekBudget.MixProject do
  use Mix.Project

  def project do
    [
      app: :week_budget,
      version: "0.1.0",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {WeekBudget.Core.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:raxx, "~> 1.0.1"},
      {:raxx_logger, "~> 0.2.2"},
      {:ace, "~> 0.18.8"},
      {:ecto, "~> 3.1.4"},
      {:ecto_sql, "~> 3.1.3"},
      {:jason, "~> 1.1.2"},
      {:observer_cli, "~> 1.5.0"},
      {:postgrex, ">= 0.0.0"},
      {:ex_money, "~> 3.4.2"},
      {:exsync, "~> 0.2.3", only: :dev}
    ]
  end
end
